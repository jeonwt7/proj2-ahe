#include <stdio.h> // for stderr
#include <stdlib.h> // for exit()
#include "types.h"
#include "utils.h"
#include "riscv.h"

void execute_rtype(Instruction, Processor *);
void execute_itype_except_load(Instruction, Processor *);
void execute_branch(Instruction, Processor *);
void execute_jal(Instruction, Processor *);
void execute_jalr(Instruction, Processor *);
void execute_load(Instruction, Processor *, Byte *);
void execute_store(Instruction, Processor *, Byte *);
void execute_ecall(Processor *, Byte *);
void execute_lui(Instruction, Processor *);
void execute_auipc(Instruction, Processor *);

void execute_instruction(uint32_t instruction_bits, Processor *processor,Byte *memory) {
    Instruction instruction = parse_instruction(instruction_bits); // Look in utils.c
    switch(instruction.opcode) {
        case 0x33:
            instruction.rtype.rd = (instruction_bits & 0xF80) >> 7;
            instruction.rtype.funct3 = (instruction_bits & 0x7000) >> 12;
            instruction.rtype.rs1 = (instruction_bits & 0xF8000) >> 15;
            instruction.rtype.rs2 = (instruction_bits & 0x1F00000) >> 20;
            instruction.rtype.funct7 = (instruction_bits & 0xFE000000) >> 25;
            execute_rtype(instruction, processor);
            break;
        case 0x13:
            instruction.itype.rd = (instruction_bits & 0xF80) >> 7;
            instruction.itype.funct3 = (instruction_bits & 0x7000) >> 12;
            instruction.itype.rs1 = (instruction_bits & 0xF8000) >> 15;
            instruction.itype.imm = (instruction_bits & 0xFFF00000) >> 20;
            execute_itype_except_load(instruction, processor);
            break;
        case 0x3:
            instruction.itype.rd = (instruction_bits & 0xF80) >> 7;
            instruction.itype.funct3 = (instruction_bits & 0x7000) >> 12;
            instruction.itype.rs1 = (instruction_bits & 0xF8000) >> 15;
            instruction.itype.imm = (instruction_bits & 0xFFF00000) >> 20;
            execute_load(instruction, processor, memory);
            break;
        case 0x67:
            instruction.itype.rd = (instruction_bits & 0xF80) >> 7;
            instruction.itype.funct3 = (instruction_bits & 0x7000) >> 12;
            instruction.itype.rs1 = (instruction_bits & 0xF8000) >> 15;
            instruction.itype.imm = (instruction_bits & 0xFFF00000) >> 20;
            execute_jalr(instruction, processor);
            break;
        case 0x23:
            instruction.stype.imm5 = (instruction_bits & 0xF80) >> 7;
            instruction.stype.funct3 = (instruction_bits & 0x7000) >> 12;
            instruction.stype.rs1 = (instruction_bits & 0xF8000) >> 15;
            instruction.stype.rs2 = (instruction_bits & 0x1F00000) >> 20;
            instruction.stype.imm7 = (instruction_bits & 0xFE000000) >> 25;
            execute_store(instruction, processor, memory);
            break;
        case 0x63:
            instruction.sbtype.imm5 = (instruction_bits & 0xF80) >> 7;
            instruction.sbtype.funct3 = (instruction_bits & 0x7000) >> 12;
            instruction.sbtype.rs1 = (instruction_bits & 0xF8000) >> 15;
            instruction.sbtype.rs2 = (instruction_bits & 0x1F00000) >> 20;
            instruction.sbtype.imm7 = (instruction_bits & 0xFE000000) >> 25;
            execute_branch(instruction, processor);
            break;
        case 0x37:
            instruction.utype.rd = (instruction_bits & 0xF80) >> 7;
            instruction.utype.imm = (instruction_bits & 0xFFFFF000) >> 12;
            execute_lui(instruction, processor);
            break;
        case 0x17:
            instruction.utype.rd = (instruction_bits & 0xF80) >> 7;
            instruction.utype.imm = (instruction_bits & 0xFFFFF000) >> 12;
            execute_auipc(instruction, processor);
            break;
        case 0x6F:
            instruction.ujtype.rd = (instruction_bits & 0xF80) >> 7;
            instruction.ujtype.imm = (instruction_bits & 0xFFFFF000) >> 12;
            execute_jal(instruction, processor);
            break;
        case 0x73:
            execute_ecall(processor, memory);
            break;
        default: // undefined opcode
            handle_invalid_instruction(instruction);
            break;
    }
}

void execute_rtype(Instruction instruction, Processor *processor) {

    Register *R = processor->R;
    unsigned int rd = instruction.rtype.rd;
    unsigned int rs1 = instruction.rtype.rs1;
    unsigned int rs2 = instruction.rtype.rs2;

    processor->PC += 4;

    Double unsigned_product;
    sDouble signed_product;

    int signed_rs1 = (int) R[rs1];
    int signed_rs2 = (int) R[rs2];
    unsigned int unsigned_rs2_lower5 = (signed_rs2 & 0x0000001F);

    switch (instruction.rtype.funct3) {
        case 0x0:
            switch (instruction.rtype.funct7) {
                case 0x00: // add
                    R[rd] = R[rs1] + R[rs2];
                    break;
                case 0x20: // sub
                    R[rd] = R[rs1] - R[rs2];
                    break;
                case 0x01: // mul
                    R[rd] = R[rs1] * R[rs2];
                    break;
            }
            break;
        case 0x1:
            switch (instruction.rtype.funct7) {
                case 0x00: // sll
                    R[rd] = R[rs1] << R[rs2];
                    break;
                case 0x01: // mulh
                    signed_product = signed_rs1 * signed_rs2;
                    R[rd] = signed_product >> 32;
                    break;
            }
            break;
        case 0x2: // slt
            if (signed_rs1 < signed_rs2) {
                R[rd] = 1;
            } else R[rd] = 0;
            break;
        case 0x3:
            switch (instruction.rtype.funct7) {
                case 0x00: // sltu
                    if (R[rs1] < R[rs2]) {
                        R[rd] = 1;
                    } else R[rd] = 0;
                    break;
                case 0x01: // mulhu
                    unsigned_product = R[rs1] * R[rs2];
                    R[rd] = unsigned_product >> 32;
                    break;
            }
            break;
        case 0x4:
            switch (instruction.rtype.funct7) {
                case 0x00: // xor
                    R[rd] = R[rs1] ^ R[rs2];
                    break;
                case 0x01: // div
                    R[rd] = signed_rs1 / signed_rs2;
            }
            break;
        case 0x5:
            switch (instruction.rtype.funct7) {
                case 0x00: // srl
                    R[rd] = R[rs1] >> R[rs2];
                    break;
                case 0x20: // sra
                    R[rd] = sign_extend_number(R[rs1] >> unsigned_rs2_lower5, 32 - unsigned_rs2_lower5);
                    break;
                case 0x01: // divu
                    R[rd] = R[rs1] / R[rs2];
                    break;
            }
            break;
        case 0x6:
            switch (instruction.rtype.funct7) {
                case 0x00: // or
                    R[rd] = R[rs1] | R[rs2];
                    break;
                case 0x01: // rem
                    R[rd] = signed_rs1 % signed_rs2;
                    break;
            }
            break;
        case 0x7:
            switch (instruction.rtype.funct7) {
                case 0x00: // and
                    R[rd] = R[rs1] & R[rs2];
                    break;
                case 0x01: // remu
                    R[rd] = R[rs1] % R[rs2];
                    break;
            }
            break;
        default:
            handle_invalid_instruction(instruction);
            exit(-1);
            break;
    }
}

void execute_itype_except_load(Instruction instruction, Processor *processor) {

    Register *R = processor->R;
    unsigned int rd = instruction.itype.rd;
    unsigned int rs1 = instruction.itype.rs1;
    int signed_imm = sign_extend_number(instruction.itype.imm, 12);
    unsigned int unsigned_imm = instruction.itype.imm;

    processor->PC += 4;

    switch (instruction.itype.funct3) {
        case 0x0: // addi
            R[rd] = R[rs1] + signed_imm;
            break;
        case 0x1: // slli
            R[rd] = R[rs1] << unsigned_imm;
            break;
        case 0x2: // slti
            if ((int) R[rs1] < signed_imm) {
                R[rd] = 1;
            } else R[rd] = 0;
            break;
        case 0x3: // sltiu
            if (R[rs1] < unsigned_imm) {
                R[rd] = 1;
            } else R[rd] = 0;
            break;
        case 0x4: // xori
            R[rd] = R[rs1] ^ signed_imm;
            break;
        case 0x5:
            switch (instruction.itype.imm >> 5) {
                case 0x00: // srli
                    R[rd] = R[rs1] >> unsigned_imm;
                    // R[rd] = R[rd] & 0xf; // incorrect ?????
                    break;
                case 0x20: // srai
                    R[rd] = sign_extend_number(R[rs1] >> unsigned_imm, 32 - unsigned_imm);
                    break;
            }
            break;
        case 0x6: // ori
            R[rd] = R[rs1] | signed_imm;
            break;
        case 0x7: // andi
            R[rd] = R[rs1] & signed_imm;
            break;
        default:
            handle_invalid_instruction(instruction);
            break;
    }
}

void execute_ecall(Processor *p, Byte *memory) {

    Register i;
    p->PC += 4;

    // What do we switch on?
    switch(p->R[10]) { // memory nnono
        case 1: // print an integer
            printf("%d",p->R[11]);
            break;
        case 4: // print a string
            for(i = p->R[11]; i < MEMORY_SPACE && load(memory, i, LENGTH_BYTE); i++) {
                printf("%c",load(memory,i,LENGTH_BYTE));
            }
            break;
        case 10: // exit
            printf("exiting the simulator\n");
            exit(0);
            break;
        case 11: // print a character
            printf("%c",p->R[11]);
            break;
        default: // undefined ecall
            printf("Illegal ecall number %d\n", p->R[10]);
            exit(-1);
            break;
    }
}

void execute_branch(Instruction instruction, Processor *processor) {

    Register *R = processor->R;
    unsigned int rs1 = instruction.sbtype.rs1;
    unsigned int rs2 = instruction.sbtype.rs2;
    int signed_rs1 = (int) R[rs1];
    int signed_rs2 = (int) R[rs2];

    switch (instruction.sbtype.funct3) {
        case 0x0: // beq
            if (R[rs1] == R[rs2]) {
                processor->PC = processor->PC + get_branch_offset(instruction);
            } else processor->PC += 4;
            break;
        case 0x1: // bne
            if (R[rs1] != R[rs2]) {
                processor->PC = processor->PC + get_branch_offset(instruction);
            } else processor->PC += 4;
            break;
        case 0x4: // blt
            if (signed_rs1 < signed_rs2) {
                processor->PC = processor->PC + get_branch_offset(instruction);
            } else processor->PC += 4;
            break;
        case 0x5: // bge
            if (signed_rs1 >= signed_rs2) {
                processor->PC = processor->PC + get_branch_offset(instruction);
            } else processor->PC += 4;
            break;
        case 0x6: // bltu
            if (R[rs1] < R[rs2]) {
                processor->PC = processor->PC + get_branch_offset(instruction);
            } else processor->PC += 4;
            break;
        case 0x7: // bgeu
            if (R[rs1] >= R[rs2]) {
                processor->PC = processor->PC + get_branch_offset(instruction);
            } else processor->PC += 4;
            break;
        default:
            handle_invalid_instruction(instruction);
            exit(-1);
            break;
    }
}

void execute_load(Instruction instruction, Processor *processor, Byte *memory) {
    Register *R = processor->R;
    unsigned int rd = instruction.itype.rd;
    unsigned int rs1 = instruction.itype.rs1;
    int offset = sign_extend_number(instruction.itype.imm, 12);
    processor->PC += 4;
    switch (instruction.itype.funct3) {
        case 0x0: // lb
            R[rd] = sign_extend_number(load(memory, R[rs1] + offset, LENGTH_BYTE), 8);
            break;
        case 0x1: // lh
            R[rd] = sign_extend_number(load(memory, R[rs1] + offset, LENGTH_HALF_WORD), 16);
            break;
        case 0x2: // lw
            R[rd] = load(memory, R[rs1] + offset, LENGTH_WORD);
            break;
        case 0x4: // lbu
            R[rd] = load(memory, R[rs1] + offset, LENGTH_BYTE);
            break;
        case 0x5: // lhu
            R[rd] = load(memory, R[rs1] + offset, LENGTH_HALF_WORD);
            break;
        default:
            handle_invalid_instruction(instruction);
            break;
    }
}

void execute_store(Instruction instruction, Processor *processor, Byte *memory) {
    Register *R = processor->R;
    unsigned int rs1 = instruction.stype.rs1;
    unsigned int rs2 = instruction.stype.rs2;
    int offset = get_store_offset(instruction);
    processor->PC += 4;
    switch (instruction.stype.funct3) {
        case 0x0: // sb
            store(memory, R[rs1] + offset, LENGTH_BYTE, R[rs2]);
            break;
        case 0x1: // sh
            store(memory, R[rs1] + offset, LENGTH_HALF_WORD, R[rs2]);
            break;
        case 0x2: // sw
            store(memory, R[rs1] + offset, LENGTH_WORD, R[rs2]);
            break;
        default:
            handle_invalid_instruction(instruction);
            exit(-1);
            break;
    }
}

void execute_jal(Instruction instruction, Processor *processor) {
  processor->R[instruction.ujtype.rd] = processor->PC + 4;
  processor->PC += get_jump_offset(instruction);
}

void execute_jalr(Instruction instruction, Processor *processor) {
    processor->R[instruction.itype.rd] = processor->PC + 4;
    processor->PC = processor->R[instruction.itype.rs1] + instruction.itype.imm;
}

void execute_lui(Instruction instruction, Processor *processor) {
    processor->R[instruction.utype.rd] = instruction.utype.imm << 12;
    processor->PC += 4;
}

void execute_auipc(Instruction instruction, Processor *processor) {
    processor->R[instruction.utype.rd] = processor->PC + (instruction.utype.imm << 12);
    processor->PC += 4;
}

void store(Byte *memory, Address address, Alignment alignment, Word value) {
    Byte byte_to_store;
    for (int i = 0; i < alignment; i++) {
        byte_to_store = value & 0x000000FF;
        memory[address + i] = byte_to_store;
        value = value >> 8;
    }
}

Word load(Byte *memory, Address address, Alignment alignment) {
    Word instruction_bits = 0;
    Word value = 0;
    for (int i = 0; i < alignment; i++) {
        value = memory[address + i];
        value = value << (8 * i);
        instruction_bits += value;
    }
    return instruction_bits;
}
