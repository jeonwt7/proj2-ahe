#include <stdio.h> // for stderr
#include <stdlib.h> // for exit()
#include "types.h"
#include "utils.h"

void print_rtype(char *, Instruction);
void print_itype_except_load(char *, Instruction, int);
void print_load(char *, Instruction);
void print_store(char *, Instruction);
void print_branch(char *, Instruction);
void print_utype(char*, Instruction);
void print_jal(Instruction);
void print_ecall(Instruction);
void write_rtype(Instruction);
void write_itype_except_load(Instruction);
void write_load(Instruction);
void write_store(Instruction);
void write_branch(Instruction);

void decode_instruction(uint32_t instruction_bits) {

    Instruction instruction = parse_instruction(instruction_bits); // Look in utils.c
    switch(instruction.opcode) {
        case 0x33: // 0110011
            instruction.rtype.rd = (instruction_bits & 0xF80) >> 7;
            instruction.rtype.funct3 = (instruction_bits & 0x7000) >> 12;
            instruction.rtype.rs1 = (instruction_bits & 0xF8000) >> 15;
            instruction.rtype.rs2 = (instruction_bits & 0x1F00000) >> 20;
            instruction.rtype.funct7 = (instruction_bits & 0xFE000000) >> 25;
            write_rtype(instruction);
            break;
        case 0x13: // 0010011
            instruction.itype.rd = (instruction_bits & 0xF80) >> 7;
            instruction.itype.funct3 = (instruction_bits & 0x7000) >> 12;
            instruction.itype.rs1 = (instruction_bits & 0xF8000) >> 15;
            instruction.itype.imm = (instruction_bits & 0xFFF00000) >> 20;
            write_itype_except_load(instruction);
            break;
        case 0x3: // 0000011
            instruction.itype.rd = (instruction_bits & 0xF80) >> 7;
            instruction.itype.funct3 = (instruction_bits & 0x7000) >> 12;
            instruction.itype.rs1 = (instruction_bits & 0xF8000) >> 15;
            instruction.itype.imm = (instruction_bits & 0xFFF00000) >> 20;
            write_load(instruction);
            break;
        case 0x67: // 1100111 - jalr only
            instruction.itype.rd = (instruction_bits & 0xF80) >> 7;
            instruction.itype.funct3 = (instruction_bits & 0x7000) >> 12;
            instruction.itype.rs1 = (instruction_bits & 0xF8000) >> 15;
            instruction.itype.imm = (instruction_bits & 0xFFF00000) >> 20;
            printf(ITYPE_FORMAT, "jalr", instruction.itype.rd, instruction.itype.rs1, sign_extend_number(instruction.itype.imm, 12));
            break;
        case 0x23: // 0100011
            instruction.stype.imm5 = (instruction_bits & 0xF80) >> 7;
            instruction.stype.funct3 = (instruction_bits & 0x7000) >> 12;
            instruction.stype.rs1 = (instruction_bits & 0xF8000) >> 15;
            instruction.stype.rs2 = (instruction_bits & 0x1F00000) >> 20;
            instruction.stype.imm7 = (instruction_bits & 0xFE000000) >> 25;
            write_store(instruction);
            break;
        case 0x63: // 1100011
            instruction.sbtype.imm5 = (instruction_bits & 0xF80) >> 7;
            instruction.sbtype.funct3 = (instruction_bits & 0x7000) >> 12;
            instruction.sbtype.rs1 = (instruction_bits & 0xF8000) >> 15;
            instruction.sbtype.rs2 = (instruction_bits & 0x1F00000) >> 20;
            instruction.sbtype.imm7 = (instruction_bits & 0xFE000000) >> 25;
            write_branch(instruction);
            break;
        case 0x37: // 0110111 - lui only
            instruction.utype.rd = (instruction_bits & 0xF80) >> 7;
            instruction.utype.imm = (instruction_bits & 0xFFFFF000) >> 12;
            print_utype("lui", instruction);
            break;
        case 0x17: // 0010111 - auipc
            instruction.utype.rd = (instruction_bits & 0xF80) >> 7;
            instruction.utype.imm = (instruction_bits & 0xFFFFF000) >> 12;
            print_utype("auipc", instruction);
            break;
        case 0x6F: // 1101111
            instruction.ujtype.rd = (instruction_bits & 0xF80) >> 7;
            instruction.ujtype.imm = (instruction_bits & 0xFFFFF000) >> 12;
            print_jal(instruction);
            break;
        case 0x73: // 1110011
            print_ecall(instruction);
            break;
        // case 0x3B:
            // w = TRUE;
            // break;
        default: // undefined opcode
            handle_invalid_instruction(instruction);
            break;
    }
}

void write_rtype(Instruction instruction) {
    /* HINT: Hmmm, it's seems that there's way more R-Type instructions that funct3 possibilities... */
    switch (instruction.rtype.funct3) {
        case 0x0:
            switch (instruction.rtype.funct7) {
                case 0x00:
                    print_rtype("add", instruction);
                    break;
                case 0x20:
                    print_rtype("sub", instruction);
                    break;
                case 0x01:
                    print_rtype("mul", instruction);
                    break;
            }
            break;
        case 0x1:
            switch (instruction.rtype.funct7) {
                case 0x00:
                    print_rtype("sll", instruction);
                    break;
                case 0x01:
                    print_rtype("mulh", instruction);
                    break;
            }
            break;
        case 0x2:
            print_rtype("slt", instruction);
            break;
        case 0x3:
            switch (instruction.rtype.funct7) {
                case 0x00:
                    print_rtype("sltu", instruction);
                    break;
                case 0x01:
                    print_rtype("mulhu", instruction);
            }
            break;
        case 0x4:
            switch (instruction.rtype.funct7) {
                case 0x00:
                    print_rtype("xor", instruction);
                    break;
                case 0x01:
                    print_rtype("div", instruction);
            }
            break;
        case 0x5:
            switch (instruction.rtype.funct7) {
                case 0x00:
                    print_rtype("srl", instruction);
                    break;
                case 0x20:
                    print_rtype("sra", instruction);
                    break;
                case 0x01:
                    print_rtype("divu", instruction);
                    break;
            }
            break;
        case 0x6:
            switch (instruction.rtype.funct7) {
                case 0x00:
                    print_rtype("or", instruction);
                    break;
                case 0x01:
                    print_rtype("rem", instruction);
            }
            break;
        case 0x7:
            switch (instruction.rtype.funct7) {
                case 0x00:
                    print_rtype("and", instruction);
                    break;
                case 0x01:
                    print_rtype("remu", instruction);
            }
            break;
        default:
            handle_invalid_instruction(instruction);
            break;
    }
}

void write_itype_except_load(Instruction instruction) {
    switch (instruction.itype.funct3) {
        case 0x0:
            print_itype_except_load("addi", instruction, sign_extend_number(instruction.itype.imm, 12));
            break;
        case 0x1:
            print_itype_except_load("slli", instruction, instruction.itype.imm & 0x1F);
            break;
        case 0x2:
            print_itype_except_load("slti", instruction, sign_extend_number(instruction.itype.imm, 12));
            break;
        case 0x3:
            print_itype_except_load("sltiu", instruction, sign_extend_number(instruction.itype.imm, 12));
            break;
        case 0x4:
            print_itype_except_load("xori", instruction, sign_extend_number(instruction.itype.imm, 12));
            break;
        case 0x5:
            /* HINT: What makes the immediate here special? */
            switch (instruction.itype.imm >> 5) {
                case 0x00:
                    print_itype_except_load("srli", instruction, instruction.itype.imm & 0x1F);
                    break;
                case 0x20:
                    print_itype_except_load("srai", instruction, instruction.itype.imm & 0x1F);
                    break;
            }
            break;
        case 0x6:
            print_itype_except_load("ori", instruction, sign_extend_number(instruction.itype.imm, 12));
            break;
        case 0x7:
            print_itype_except_load("andi", instruction, sign_extend_number(instruction.itype.imm, 12));
            break;
        default:
            handle_invalid_instruction(instruction);
            break;
    }
}

void write_load(Instruction instruction) {
    switch (instruction.itype.funct3) {
        case 0x0:
            print_load("lb", instruction);
            break;
        case 0x1:
            print_load("lh", instruction);
            break;
        case 0x2:
            print_load("lw", instruction);
            break;
        case 0x4:
            print_load("lbu", instruction);
            break;
        case 0x5:
            print_load("lhu", instruction);
            break;
        default:
            handle_invalid_instruction(instruction);
            break;
    }
}

void write_store(Instruction instruction) {
    switch (instruction.stype.funct3) {
        case 0x0:
            print_store("sb", instruction);
            break;
        case 0x1:
            print_store("sh", instruction);
            break;
        case 0x2:
            print_store("sw", instruction);
            break;
        default:
            handle_invalid_instruction(instruction);
            break;
    }
}

void write_branch(Instruction instruction) {
    switch (instruction.sbtype.funct3) {
        case 0x0:
            print_branch("beq", instruction);
            break;
        case 0x1:
            print_branch("bne", instruction);
            break;
        case 0x4:
            print_branch("blt", instruction);
            break;
        case 0x5:
            print_branch("bge", instruction);
            break;
        case 0x6:
            print_branch("bltu", instruction);
            break;
        case 0x7:
            print_branch("bgeu", instruction);
            break;
        default:
            handle_invalid_instruction(instruction);
            break;
    }
}

/* utils.c and utils.h might be useful here... */

void print_utype(char* name, Instruction instruction) {
    printf(UTYPE_FORMAT, name, instruction.utype.rd, instruction.utype.imm); // "%s\tx%d, %d\n"
}

void print_jal(Instruction instruction) {
    printf(JAL_FORMAT, instruction.ujtype.rd, get_jump_offset(instruction)); // "jal\tx%d, %d\n"
}

void print_ecall(Instruction instruction) {
    printf(ECALL_FORMAT); // "ecall\n"
}

void print_rtype(char *name, Instruction instruction) {
    printf(RTYPE_FORMAT, name, instruction.rtype.rd, instruction.rtype.rs1, instruction.rtype.rs2); // "%s\tx%d, x%d, x%d\n"
}

void print_itype_except_load(char *name, Instruction instruction, int imm) {
    printf(ITYPE_FORMAT, name, instruction.itype.rd, instruction.itype.rs1, imm); // "%s\tx%d, x%d, %d\n"
}

void print_load(char *name, Instruction instruction) {
    printf(MEM_FORMAT, name, instruction.itype.rd, sign_extend_number(instruction.itype.imm, 12), instruction.itype.rs1); // "%s\tx%d, %d(x%d)\n"
}

void print_store(char *name, Instruction instruction) {
    printf(MEM_FORMAT, name, instruction.stype.rs2, get_store_offset(instruction), instruction.stype.rs1); //
}

void print_branch(char *name, Instruction instruction) {
    printf(BRANCH_FORMAT, name, instruction.sbtype.rs1, instruction.sbtype.rs2, get_branch_offset(instruction)); // "%s\tx%d, x%d, %d\n"
}
