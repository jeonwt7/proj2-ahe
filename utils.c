#include "utils.h"
#include <stdio.h>
#include <stdlib.h>

/* Sign extends the given field to a 32-bit integer where field is
 * interpreted an n-bit integer. Look in test_utils.c for examples. */
int sign_extend_number( unsigned int field, unsigned int n) {
    if (field >> (n - 1) == 1) { // negative
        return (0xFFFFFFFF << n) + field;
    }
    return field;
}

/* Unpacks the 32-bit machine code instruction given into the correct
 * type within the instruction struct. Look at types.h */
Instruction parse_instruction(uint32_t instruction_bits) {
    Instruction instruction;
    instruction.opcode = instruction_bits & 0x7F;
    instruction.rest = (instruction_bits & 0xFFFFFF80) >> 7;
    return instruction;
}

/* Return the number of bytes (from the current PC) to the branch label using the given
 * branch instruction */
int get_branch_offset(Instruction instruction) { // 12|10:5, 4:1|11
    return sign_extend_number(
          ((instruction.sbtype.imm5 >> 1) << 1) + // 4:1
          ((instruction.sbtype.imm5 & 1) << 11) + // 11
          ((instruction.sbtype.imm7 & 0x3F) << 5) + // 10:5
          ((instruction.sbtype.imm7 & 0x40) << 6), 13); // 12
}

/* Returns the number of bytes (from the current PC) to the jump label using the given
 * jump instruction */
int get_jump_offset(Instruction instruction) { // 20|10:1|11|19:12
    return sign_extend_number(
          ((instruction.ujtype.imm & 0xFF) << 12) + // 19:12
          ((instruction.ujtype.imm & 0x100) << 3) + // 11
          ((instruction.ujtype.imm & 0x7FE00) >> 8) + // 10:1
          ((instruction.ujtype.imm & 0x80000) << 1), 21); // 12
}

/* Returns the byte offset (from the address in rs2) for storing info using the given
 * store instruction */
int get_store_offset(Instruction instruction) {
    return sign_extend_number((instruction.stype.imm7 << 5) + instruction.stype.imm5, 12);
}

void handle_invalid_instruction(Instruction instruction) {
    printf("Invalid Instruction: 0x%08x\n", instruction.bits); 
    exit(-1);
}

void handle_invalid_read(Address address) {
    printf("Bad Read. Address: 0x%08x\n", address);
    exit(-1);
}

void handle_invalid_write(Address address) {
    printf("Bad Write. Address: 0x%08x\n", address);
    exit(-1);
}
